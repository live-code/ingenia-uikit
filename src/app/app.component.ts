import { Component, ElementRef, OnChanges, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'ing-root',
  template: `
    
    <!--<ing-demo1></ing-demo1>-->
    <!--<ing-demo2></ing-demo2>-->
    <ing-demo3></ing-demo3>
  `,
})
export class AppComponent {

}

import { Directive, ElementRef, HostBinding, Renderer2 } from '@angular/core';
import { AuthService } from '../demo3.component';

@Directive({
  selector: '[ingIflogged]'
})
export class IfloggedDirective {
/*  @HostBinding('style.display') get display(): string {
    console.log('render')
    return this.authService.isLogged ? null : 'none';
  }*/

  constructor(
    private authService: AuthService,
    private el: ElementRef,
    private renderer2: Renderer2
  ) {
    authService.isLogged$
      .subscribe(isLogged => {
        if (isLogged) {
          this.renderer2.setStyle(el.nativeElement, 'display', null)
        } else {
          this.renderer2.setStyle(el.nativeElement, 'display', 'none')
        }
      });
  }
}

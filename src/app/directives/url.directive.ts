import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[ingUrl]'
})
export class UrlDirective {
  @Input() ingUrl: string;

  @HostListener('click')
  clickMe(): void {
    window.open(this.ingUrl);
  }

}

import { Component, Injectable, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root'})
export class AuthService {
  isLogged$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false)
  role$: BehaviorSubject<string> = new BehaviorSubject<string>(null)
  isLogged = false;

  constructor() {
    setTimeout(() => {
      this.role$.next('editor');
      this.isLogged$.next(true);

    }, 2000)
  }

  login(): void {
    this.isLogged = true;
    this.role$.next('admin');
    this.isLogged$.next(true);
  }
}

@Component({
  selector: 'ing-demo3',
  template: `
   
    <div ingAlert="danger">bla bla bla</div>
    <div [ingAlert]="alertType" [border]="borderWidth">bla bla bla</div>
    
    <button ingIflogged
      ingBg="red"
      (click)="borderWidth = 10">10</button>
    <button ingIflogged
      ingBg="cyan"
      (click)="borderWidth = 40">40</button>
    
    <hr>
    <h3>Login simulator</h3>
    <button (click)="authService.login()">login</button>
    <div ingIflogged>SOno loggato</div>
    <div ingIfrole="admin">SOno Figo sono admin</div>
    <div ingIfrole="editor">SOno un semplice editor</div>
    
    <hr>
    <button ingUrl="http://www.google.com">Google</button>
    <div ingUrl="http://www.google.com">Google</div>
  `,
})
export class Demo3Component {
  alertType = 'success';
  borderWidth = 10;

  constructor(public authService: AuthService) {
  }
}


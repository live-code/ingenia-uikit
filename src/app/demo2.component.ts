import { Component, OnInit } from '@angular/core';
import { City, Country } from './model/country';
import { TabbarComponent } from './components/tabbar.component';

@Component({
  selector: 'ing-demo2',
  template: `
    <ing-tabbar 
      [data]="countries"
      [active]="activeCountry"
      icon="fa fa-link"
      (tabClick)="setActiveCountry($event)"
      (iconClick)="openUrl($event)"
    ></ing-tabbar>

    <ing-tabbar
      [data]="activeCountry?.cities"
      [active]="activeCity"
      labelField="label"
      icon="fa fa-trash"
      (tabClick)="setActiveCity($event)"
      (iconClick)="deleteCity($event)"
    ></ing-tabbar>
   
    <ing-google-map
      *ngIf="activeCity?.coords"
      [coords]="activeCity?.coords"></ing-google-map>
    
    <button (click)="setActiveCountry(countries[2])">goto UK</button>
    
    <pre>{{countries | json}}</pre>
  `,
})
export class Demo2Component {
  countries: Country[] = [
    {
      id: 1,
      name: 'Italy',
      cities: [
        { id: 1001, label: 'Milan', coords: { lat: 41, lng: 12 } },
        { id: 1002, label: 'Rome', coords: { lat: 42, lng: 13 } } ,
        { id: 1003, label: 'Palermo', coords: { lat: 43, lng: 14 } }
      ]

    },
    {
      id: 2,
      name: 'Germany',
      cities: [
        { id: 1001, label: 'Berlino', coords: { lat: 55, lng: 33 } },
      ]
    },
    {
      id: 3,
      name: 'UK',
      cities: [
        { id: 1004, label: 'London', coords: { lat: 66, lng: 22 } },
        { id: 1007, label: 'Leeds', coords: { lat: 11, lng: 22 } },
      ]
    },
  ];
  activeCountry: Country;
  activeCity: City;

  constructor() {
    this.setActiveCountry( this.countries[0] );

    setTimeout(() => {
      this.deleteCity(this.countries[0].cities[0])
    }, 2000)
  }

  setActiveCountry(country: Country): void {
    this.activeCountry = country;
    this.activeCity = this.activeCountry.cities[0];
  }

  setActiveCity(city: City): void {
    this.activeCity = city;
  }

  openUrl(country: Country): void {
    window.open('https://it.wikipedia.org/wiki/' + country.name)
  }

  deleteCity(cityToRemove: City): void {

    // update countries
    this.countries = this.countries.map(country => {
      if (country.id === this.activeCountry.id) {
        return {
          ...country,
          cities: country.cities.filter(city => city.id !== cityToRemove.id)
        };
      }
      return country;
    });


    // Update active city
    const countryIndex = this.countries.findIndex(c => c.id === this.activeCountry.id);
    this.activeCountry = {
      ...this.countries[countryIndex],
      cities: this.activeCountry.cities.filter(city => city.id !== cityToRemove.id)
    };

    /*this.countries = [...newCountries];
    this.activeCountry = {...newCountries[countryIndex2]};
    console.log(this.countries, newCountries)*/
  }
}

type Product = {
  cities: any
  names: any
};

const obj: Product = {
  cities: {
    name: 'pippo'
  },
  names: {
    another: true
  }
};

console.log('keys', Object.keys(obj));
console.log('values', Object.values(obj));
/*for (const item of Object.values(obj)) {
  console.log(item);
}*/

obj.cities = { name: 'ciao'};
obj.names = { name: 'ciao'};
// obj.xxx = { name: 'ciao'}; // error


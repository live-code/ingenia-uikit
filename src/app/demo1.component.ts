import { Component, ElementRef, OnChanges, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'ing-demo1',
  template: `
    
    <ing-google-map [zoom]="zoom" [coords]="coords"></ing-google-map>
    <button (click)="zoom = zoom - 1">-</button>
    <button (click)="zoom = zoom + 1">+</button>
    <button (click)="coords = { lat: 44, lng: 12}">44</button>
    <button (click)="coords = { lat: 45, lng: 12}">45</button>
    
    <ing-chartjs
      [lineColor]="lineColor"
      [temps]="temps"></ing-chartjs>
    <button (click)="temps = [11, 12, 32, 4, 4, 423, 32]">Temp 1</button>
    <button (click)="temps = [33, 24, 32, 44, 14, 1, 32]">Temp 2</button>
    <button (click)="lineColor = 'green'">green</button>
    <button (click)="lineColor = 'cyan'">cyan</button>
    <hr>
    <ing-weather
      [city]="city"
      [unit]="unit"
    ></ing-weather>
    
    <button (click)="city = 'Trieste'">Trieste</button>
    <button (click)="city = 'Torin'">Torin</button>
    <button (click)="unit = 'metric'">metric</button>
    <button (click)="unit = 'imperial'">imperial</button>
  `,
})
export class Demo1Component {
  temps = [111, 132, 342, 45, 45, 23, 32];
  lineColor = 'pink';
  unit = 'metric';
  city = 'Milan'
  zoom = 5;
  coords = { lat: 43, lng: 12};
}

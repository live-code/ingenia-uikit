import { Directive, ElementRef, HostBinding, Input, Renderer2 } from '@angular/core';
import { AuthService } from '../demo3.component';
import { withLatestFrom } from 'rxjs/operators';

@Directive({
  selector: '[ingIfrole]'
})
export class IfroleDirective {
  @Input() ingIfrole: string;

  constructor(
    private authService: AuthService,
    private el: ElementRef,
    private renderer2: Renderer2
  ) {
    authService.isLogged$
      .pipe(
        withLatestFrom(authService.role$)
      )
      .subscribe(([isLogged, myRole]) => {
        if (isLogged && myRole === this.ingIfrole) {
          this.renderer2.setStyle(el.nativeElement, 'display', null)
        } else {
          this.renderer2.setStyle(el.nativeElement, 'display', 'none')
        }
      });
  }
}

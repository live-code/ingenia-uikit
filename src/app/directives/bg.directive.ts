import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[ingBg]'
})
export class BgDirective {
  @Input() set ingBg(val: string) {
    // this.el.nativeElement.style.background = val;
    this.renderer2.setStyle(this.el.nativeElement, 'background', val)
  }

  constructor(
    private el: ElementRef,
    private renderer2: Renderer2
  ) {

  }

}

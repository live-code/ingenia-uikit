import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[ingAlert]'
})
export class AlertDirective {
  @Input() ingAlert: 'success' | 'danger'
  @Input() border = 10;

  @HostBinding() get className(): string {
    return 'alert alert-' + (this.ingAlert || 'danger');
  }
  @HostBinding('style.fontSize') fontSize = '40px';
  @HostBinding('style.borderWidth') get borderStyle(): string {
    return this.border + 'px';
  }


}



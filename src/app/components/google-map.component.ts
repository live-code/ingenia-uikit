import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';


@Component({
  selector: 'ing-google-map',
  template: `
    <div #host class="gmap"></div>
  `,
  styles: [`
    .gmap { width: 100%; height: 300px}
  `]
})
export class GoogleMapComponent implements OnChanges {
  @ViewChild('host', { static: true }) host: ElementRef<HTMLDivElement>;
  @Input() coords: {  lat: number; lng: number };
  @Input() zoom: number;
  map: google.maps.Map;

  init(): void {
    this.map = new google.maps.Map(this.host.nativeElement, {
      center: { lat: -34.397, lng: 150.644 },
      zoom: 5
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.map) {
      this.init();
    }
    if (changes.zoom) {
      this.map.setZoom(changes.zoom.currentValue)
    }
    if (changes.coords && changes.coords.currentValue) {
      const latlng: google.maps.LatLng = new google.maps.LatLng(changes.coords.currentValue.lat, changes.coords.currentValue.lng);
      this.map.panTo(latlng);
    }
  }


}

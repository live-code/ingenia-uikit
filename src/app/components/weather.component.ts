import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'ing-weather',
  /*changeDetection: ChangeDetectionStrategy.OnPush,*/
  template: `
    <input type="text" [formControl]="inputCity" #inputRef>
    <h1 >{{meteo?.temp}}°</h1>
    <img [src]="meteo?.icon" alt="">
  `,
})
export class WeatherComponent implements OnInit, AfterViewInit, OnChanges {
  @ViewChild('inputRef') inputRef: ElementRef<HTMLInputElement>
  inputCity = new FormControl('');
  @Input() city: string;
  @Input() unit: string;
  meteo: { temp: number; text: string; icon: string};
  icon: string;

  constructor(private http: HttpClient) {
    console.log('ctr', this.inputCity.value);
  }

  ngOnInit(): void {
    console.log('init', this.inputCity.value);
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('changes', this.inputCity.value);
    if (changes.city) {
      this.inputCity.setValue(changes.city.currentValue);
    }

    if (changes.unit) {
      this.http.get<any>(`http://api.openweathermap.org/data/2.5/weather?q=${this.inputCity.value}&units=${changes.unit.currentValue}&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
        .subscribe(res => {
          this.meteo = {
            temp: res?.main.temp,
            text: res?.weather[0].main,
            icon: 'http://openweathermap.org/img/w/' + res?.weather[0].icon + '.png'
          };
        });
    }

  }

  ngAfterViewInit(): void {
    console.log('ngAfterViewInit', this.inputCity.value);
    this.inputRef.nativeElement.focus();
  }

}

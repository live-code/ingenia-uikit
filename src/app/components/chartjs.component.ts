import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import {
  Chart,
  LineController,
  RadialLinearScale,
  RadarController,
  CategoryScale,
  LineElement,
  PointElement,
  LinearScale,
  Title,
} from 'chart.js';
import { config } from './chartjs.helper';

Chart.register(RadarController, RadialLinearScale, LineController, LineElement, CategoryScale, PointElement, LinearScale, Title);

@Component({
  selector: 'ing-chartjs',
  template: `
    <canvas #host width="400" height="400"></canvas>
  `,
})
export class ChartjsComponent implements OnChanges {
  @ViewChild('host', { static: true }) host: ElementRef<HTMLCanvasElement>;
  @Input() lineColor: string;
  @Input() temps: number[];
  myChart: Chart;

  init(): void {
    this.myChart = new Chart(this.host.nativeElement.getContext('2d'), config);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.myChart) {
      this.init();
    }
    if (changes.temps) {
      this.myChart.data.datasets[0].data = changes.temps.currentValue
    }
    if (changes.lineColor) {
      this.myChart.data.datasets[0].borderColor = changes.lineColor.currentValue;
    }
    this.myChart.update();
  }
}

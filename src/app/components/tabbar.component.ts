import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ing-tabbar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="wrapper">
      <span 
        class="tab" 
        *ngFor="let item of data"
        [ngClass]="{'active': item.id === active?.id}"
        (click)="tabClick.emit(item)"
      >
        {{item[labelField]}}
        
        <i [ngClass]="icon" (click)="iconClickHandler($event, item)"></i>
      </span>
    </div>
    {{ render()}}
  `,
  styles: [`      
    .wrapper { display: flex; }
    .tab {
      background-color: #ccc;
      padding: 1rem;
      border-bottom: 1px solid white;
    }
    .tab:not(:last-child) {
      border-right: 1px solid black;
    }
    .active {
      background-color: #222;
      color: white;
    }
  `]
})
export class TabbarComponent<T extends { id: number }> {
  @Input() data: T[];
  @Input() active: T;
  @Input() icon: string;
  @Input() labelField = 'name';
  @Output() iconClick: EventEmitter<T> = new EventEmitter<T>();
  @Output() tabClick: EventEmitter<T> = new EventEmitter<T>();

  iconClickHandler(event: MouseEvent, item: T): void {
    event.stopPropagation();
    this.iconClick.emit(item);
  }

  render() {
    console.log('render')
  }
}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WeatherComponent } from './components/weather.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GoogleMapComponent } from './components/google-map.component';
import { ChartjsComponent } from './components/chartjs.component';
import { TabbarComponent } from './components/tabbar.component';
import { Demo1Component } from './demo1.component';
import { Demo2Component } from './demo2.component';
import { Demo3Component } from './demo3.component';
import { BgDirective } from './directives/bg.directive';
import { AlertDirective } from './directives/alert.directive';
import { IfloggedDirective } from './directives/iflogged.directive';
import { IfroleDirective } from './directives/ifrole.directive';
import { UrlDirective } from './directives/url.directive';

@NgModule({
  declarations: [
    AppComponent,
    Demo1Component,
    WeatherComponent,
    GoogleMapComponent,
    ChartjsComponent,
    TabbarComponent,
    Demo2Component,
    Demo3Component,
    BgDirective,
    AlertDirective,
    IfloggedDirective,
    IfroleDirective,
    UrlDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
